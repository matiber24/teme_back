const CrocodilDb = require("../models").Crocodil;
const InsulaDb = require("../models").Insula;

const controller = {
    getAllCrocodiles: async (req, res) => {
        CrocodilDb.findAll().then((crocodili) => {
            res.status(200).send(crocodili);
        }).catch((err) => {
            console.log(err);
            res.status(500).send({ message: "server kaput" });
        })
    },

    addCrocodile: async (req, res) => {
        const { idInsula, nume, prenume, varsta } = req.body;
        InsulaDb.findByPk(idInsula)
            .then((insula) => {
                if (insula) {
                    insula.createCrocodil({ nume, prenume, varsta }).then((crocodil) => {
                        res.status(201).send({ crocodil });
                    }).catch((error) => {
                        console.log(error);
                        res.status(500).send({ message: "Server error!" });
                    })
                } else {
                    res.status(404).send("Insula not found!");
                }
            }).catch((error) => {
                console.log(error);
                res.status(500).send({ message: "Server error!" });
            });
    },

    getCrocodileById: async (req, res) => {
        const id = req.params.id;
        if (!id) {
            res.status(400).send({ message: "ID not provided" });
        } else {
            CrocodilDb.findByPk(id).then((crocodil) => {
                if (crocodil) {
                    res.status(200).send(crocodil);
                } else {
                    res.status(404).send({ message: `Croco with id: ${id} not found` });
                }
            }).catch((error) => {
                console.log(error);
                res.status(500).send({ message: "Server error!" });
            })
        }
    },

    deleteCrocodileById: async (req, res) => {
        const id = req.params.id;
        if (!id) {
            res.status(400).send({ message: "ID not provided" });
        } else {
            CrocodilDb.findByPk(id).then((crocodil) => {
                if (crocodil) {
                    crocodil.destroy().then((croco) => {
                        res.status(202).send(croco);
                    });
                } else {
                    res.status(404).send({ message: `Croco with id: ${id} not found` });
                }
            }).catch((error) => {
                console.log(error);
                res.status(500).send({ message: "Server error!" });
            });
        }
    },

    updateCrocodile: async (req, res) => {
        const id = req.params.id;
        if (!id) {
            res.status(400).send({ message: "ID not provided" });
        } else {
            CrocodilDb.findByPk(id).then((crocodil) => {
                crocodil.update(req.body).then((croco) => {
                    res.status(200).send(croco);
                });
            }).catch((error) => {
                console.log(error);
                res.status(500).send({ message: "Server error!" });
            });
        }
    }

}

module.exports = controller;