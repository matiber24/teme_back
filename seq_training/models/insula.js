module.exports = (sequelize, DataTypes) => {
    return sequelize.define("insula", {
        nume: DataTypes.STRING,
        suprafata: DataTypes.FLOAT,
    });
};