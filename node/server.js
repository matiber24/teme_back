//importuri de pachete
const express = require("express");
const mysql = require("mysql");

//initializarea aplicatiei
const app = express();

//pentru ca aplicatia sa poata citi obiectele pe care le trimitem in request
app.use(express.urlencoded({ extended: true }));
app.use(express.json());

let port = 8080;

app.listen(port, () => {
  console.log("Serverul merge pe portul " + port);
});

const connection = mysql.createConnection({
  host: "localhost",
  user: "root",
  password: "",
  database: "training_it",
});

connection.connect((err) => {
  if (err) throw err;

  console.log("Baza de date conectat");

  const sqlQuery =
    "CREATE TABLE IF NOT EXISTS Crocodili(id INTEGER PRIMARY KEY  NOT NULL AUTO_INCREMENT, nume VARCHAR(30), prenume VARCHAR(30), telefon VARCHAR(10), email VARCHAR(30), activ BOOLEAN)";

  connection.query(sqlQuery, (err) => {
    if (err) throw err;
    else console.log("Tabela crocodili creata!");
  });
});

app.post("/crocodili", (req, res) => {
  const croco = {
    nume: req.body.nume,
    prenume: req.body.prenume,
    email: req.body.email,
    telefon: req.body.telefon,
    activ: req.body.activ,
  };

  let errors = [];

  //validari
  if (!croco.nume || !croco.prenume || !croco.email || !croco.telefon || croco.activ === undefined) {
    errors.push("Unul sau mai multe campuri nu au fost completate!");
  }
  if (!croco.email.includes("@gmail.com") && !croco.email.includes("@yahoo.com")) {
    errors.push("Email incorect");
  }
  if (croco.telefon.length !== 10) {
    errors.push("Telefon incorect");
  }

  if (errors.length === 0) {
    try {
      const insertQuery = `INSERT INTO Crocodili(nume,prenume,email,telefon,activ) VALUES('${croco.nume}', '${croco.prenume}', '${croco.email}', '${croco.telefon}', '${croco.activ}')`;

      connection.query(insertQuery, (err) => {
        if (err) throw err;
        else {
          console.log("Bine ai venit, rau ai nimerit");
          res.status(201).send({ message: "Bine ai venit, rau ai nimerit" });
        }
      });
    } catch (err) {
      console.log("Server kaput");
      res.status(500).send(err);
    }
  } else {
    console.log("Eroare!");
    res.status(400).send(errors);
  }
});

app.get("/crocodili", (req, res) => {
  try {
    let select = "";
    if (req.query.activ) {
      select = `SELECT * FROM Crocodili WHERE activ = '${req.query.activ}'`;
    } else {
      select = "SELECT * FROM Crocodili";
    }
    connection.query(select, (err, result) => {
      if (err) throw err;
      res.status(200).send(result);
    });
  } catch {
    console.log("Server kaput");
    res.status(500).send(err);
  }
});

app.delete("/crocodili/:id", (req, res) => {
  try {
    const sqlDelete = `DELETE FROM Crocodili WHERE id = '${req.params.id}'`;
    connection.query(sqlDelete, (err) => {
      if (err) throw err;
      res.status(200).send({ message: "Crocodil disparut" });
    });
  } catch {
    console.log("Server kaput");
    res.status(500).send(err);
  }
});

app.get("/crocodili/:id", (req, res) => {
  try {
    const select = `SELECT * FROM Crocodili WHERE id = '${req.params.id}'`;
    console.log(req.query);
    connection.query(select, (err, result) => {
      if (err) throw err;
      res.status(200).send(result);
    });
  } catch {
    console.log("Server kaput");
    res.status(500).send(err);
  }
});

app.put("/crocodili/:id", (req, res) => {
  const cerere = {
    nume: req.body.nume,
    prenume: req.body.prenume,
    email: req.body.email,
    telefon: req.body.telefon,
    activ: req.body.activ,
  }

  let update = `UPDATE Crocodili SET `;

  for (let camp in cerere) {
    if (cerere[camp]) {
      if (typeof cerere[camp] === 'string') {
        update += `${camp} = '${cerere[camp]}',`;
      } else {
        update += `${camp} = ${cerere[camp]},`
      }
    }
  }

  if (update[update.length - 1] == ',') {

    update = update.substring(0, update.length - 1);
    update += ` WHERE id = '${req.params.id}'`;

    console.log(update);

    try {
      connection.query(update, (err, result) => {
        if (err) throw err;
        res.status(200).send("Crocodil modificat");
      });
    } catch (err) {
      console.log("Server kaput");
      res.status(500).send(err);
    }
  } else {
    console.log("Input error");
    res.status(400).send("trimite ceva an plm in budy");
  }

});
